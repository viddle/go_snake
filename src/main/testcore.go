package main

import "core"

func main () {
	g := core.Init ()

	g.VlineWall (0, 0, 25)
	g.VlineWall (0, 75, 100)

	g.VlineWall (100, 0, 25)
	g.VlineWall (100, 75, 100)

	g.HlineWall (0, 0, 25)
	g.HlineWall (0, 75, 100)

	g.HlineWall (100, 0, 25)
	g.HlineWall (100, 75, 100)

	g.MoveHead ()
	g.CleanTail ()

	g.MoveHead ()
	g.CleanTail ()
	
	g.UpdateDir (core.Up)

	g.MoveHead ()
	g.CleanTail ()

	g.PrintTyTest(core.Pos{53, 51})
	
}
	


