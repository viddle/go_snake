package main

//import "fmt"
import "github.com/hajimehoshi/ebiten"
import "core"
//import "image/color"

var s = core.Size
var pixels = make ([] byte, 4 * s * s)
var inited = false
var g = core.Init ()

func inite (screen *ebiten.Image) {
	for j := 0; j < s; j++ {
		for i := 0; i < s; i++ {
			pixels[0+4*(i+j*s)] = 255
			pixels[1+4*(i+j*s)] = 255
			pixels[2+4*(i+j*s)] = 255
			pixels[3+4*(i+j*s)] = 255
		}
	}
	for h := 0; h < 40; h++ {
		pixel (screen, core.Pos{50, h}, 0, 0, 0)
	}
}

	
func update(screen *ebiten.Image) error {
	if !inited {
		inite (screen)
		inited = true
	}

	if ebiten.IsKeyPressed (ebiten.KeyDown) {
		g.UpdateDir (core.Down)
	}
	if ebiten.IsKeyPressed (ebiten.KeyUp) {
		g.UpdateDir (core.Up)
	}
	if ebiten.IsKeyPressed (ebiten.KeyLeft) {
		g.UpdateDir (core.Left)
	}
	if ebiten.IsKeyPressed (ebiten.KeyRight) {
		g.UpdateDir (core.Right)
	}
	
	g.MoveHead ()
	pixel (screen, *g.Head, 0, 0, 0)

	g.CleanTail ()
	pixel (screen, *g.Tail, 255, 255, 255)
	

	
	return nil
}



func pixel (screen *ebiten.Image, p core.Pos, r, g, b byte) {
	pixels[0+ 4 * (p.X + s * (100 - p.Y))] = r
	pixels[1+ 4 * (p.X + s * (100 - p.Y))] = g
	pixels[2+ 4 * (p.X + s * (100 -p.Y))] = b
	screen.ReplacePixels (pixels)
}
	


func main() {
	ebiten.Run(update, 101, 101, 5, "Snake")
	
}
