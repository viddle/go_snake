package core

import "fmt"

type Pos struct {
	X int
	Y int
}

const Size = 101

type Dir int

const (
	Up Dir = iota
	Right 
	Down
	Left 
)

func opposite (d Dir) Dir {
	switch d {
	case Up : return Down
	case Right : return Left
	case Down : return Up
	case Left : return Right
	default : panic("not a good direction")
	}
}

func modulo (a, b int) int {
	r := a % b
	if r < 0 {
		return r + b
	} else {
		return r
	}
}

func next (pos Pos, dir Dir, size int) Pos {
	x := pos.X
	y := pos.Y
	switch dir {
	case Up : return Pos {x, modulo (y+1, size)}
	case Right : return Pos {modulo (x+1, size), y}
	case Down : return Pos {x, modulo (y-1, size)}
	case Left : return Pos {modulo (x-1, size), y}
	default : panic("not a good direction")
	}
}

type Ty interface {
	isTySquare ()
}

type Empty struct {}
func (c Empty) isTySquare () {}

type Food struct {}
func (c Food) isTySquare () {}

type Wall struct {}
func (c Wall) isTySquare () {}

type Snake struct {
	dir *Dir}
func (c Snake) isTySquare () {}


type Game struct {
	board [Size][Size]Ty
	Tail *Pos
	Head *Pos
}


func Init () Game {
	var board [Size][Size]Ty
	for x:= 0; x <=100; x++ {
		for y:= 0; y <=100; y++ {
			board[x][y] = Empty {}
		}
	}
	r1 := Right
	r2 := Right
	board[50][50] = Snake {&r1}
	board[51][50] = Snake {&r2}
	return Game{board, &Pos{50, 50}, &Pos{51, 50}}
}

func (g *Game) What (pos Pos) Ty {
	return g.board[pos.X][pos.Y]
}

func (g *Game) Set (pos Pos, ty Ty) {
	g.board[pos.X][pos.Y] = ty
}

func (g *Game) UpdateDir (d Dir) {
	switch h := g.What(*g.Head).(type) {
	case Snake :
		if d != opposite (*h.dir) {
			g.Set(*g.Head, Snake {&d})
		}
	default : panic ("the head is not a snake")
	}
}

func (g *Game) getDir (p Pos) Dir {
	switch c := g.What(p).(type) {
	case Snake : return *c.dir
		default :
		fmt.Println (p)
		panic ("the head is not a snake")
	}
}	

func (g *Game) Next (p Pos) Pos {
	return next (p, g.getDir(p), Size)
}

func (g *Game) MoveHead () {
	dir := g.getDir(*g.Head)
	newHead := g.Next(*g.Head)
	g.Set (newHead, Snake {&dir})
	g.Head = &newHead
}

func (g *Game) CleanTail () {
	newTail := g.Next (*g.Tail)
	g.Set (*g.Tail, Empty {})
	g.Tail = &newTail
}


func (g *Game) HlineWall (y, x1, x2 int) {
	for x:= x1; x <=x2; x++ {
		g.board[x][y] = Wall {}
	}
}

func (g *Game) VlineWall (x, y1, y2 int) {
	for y:= y1; y <=y2; y++ {
		g.board[x][y] = Wall {}
	}
}

func (g *Game) PrintTyTest (p Pos) {
	switch c := g.What(p).(type) {
	case Snake : fmt.Println ("its a snake!, dir", *c.dir)
	default : fmt.Println ("its not a snake :(")
	}
	
}


